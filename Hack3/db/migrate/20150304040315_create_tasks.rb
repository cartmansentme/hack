class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :task_name
      t.string :description
      t.boolean :complete
      t.integer :job_id

      t.timestamps null: false
    end
  end
end
