class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :job_name
      t.string :location
      t.date :due_date
      t.boolean :complete
      t.integer :employee_id
      t.integer :task_id

      t.timestamps null: false
    end
  end
end
