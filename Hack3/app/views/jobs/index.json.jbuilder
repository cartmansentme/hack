json.array!(@jobs) do |job|
  json.extract! job, :id, :job_name, :location, :due_date, :complete, :employee_id, :task_id
  json.url job_url(job, format: :json)
end
