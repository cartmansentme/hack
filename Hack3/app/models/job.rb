class Job < ActiveRecord::Base
  has_many :tasks
  belongs_to :employee
end
