class Job < ActiveRecord::Base

  belongs_to :Employee
  has_many :Tasks


  def check_completed
    self.complete = true
    tasks.each do |task|
      if task.complete != true
        self.complete = false
      end
    end
    self.save
  end

end
