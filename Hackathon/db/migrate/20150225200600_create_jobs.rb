class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.integer :job_id
      t.string :job_title
      t.string :location
      t.date :due_date

      t.timestamps null: false
    end
  end
end
